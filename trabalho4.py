import numpy as np
import matplotlib.pyplot as plt

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 

import timeit

from numba import jit, njit, prange

@njit
def residuo(x, Nx, Ny, dx, dt, omega, u_star, v_star, termo1, termo2):
	resid=np.copy(x)
	for i in range(Nx-1):
		for j in range(Ny-1):
			if(i==0):
				termo1 = (x[i+1, j] + x[i, j])/4.0
			elif(i==Nx-2):
				termo1 = (x[i, j] + x[i-1, j])/4.0
			else:
				termo1 = (x[i+1, j] + x[i-1, j])/4.0
			if(j==0):
				termo2 = (x[i,j+1] + x[i,j])/4.0
			elif(j==Ny-2):
				termo2 = (x[i,j] + x[i,j-1])/4.0
			else:
				termo2 = (x[i,j+1] + x[i, j-1])/4.0
			resid[i,j] = termo1 + termo2 - ((dx)/(4.0*dt))*(u_star[i+1,j]-u_star[i,j] + v_star[i, j+1] - v_star[i,j]) - x[i,j]
	for i in range(Nx-1):
		resid[i, -1] = resid[i, 0] + x[i,0] - x[i,-1]
		resid[i, Ny-1] = resid[i, Ny-2] + x[i, Ny-2] - x[i, Ny-1]
	for j in range(Ny-1):
		resid[-1, j] = resid[0, j] + x[0, j] - x[-1,j]
		resid[Nx-1, j] = resid[Nx-2, j] + x[Nx-2, j] - x[Nx-1,j]
	resid[-1, -1] = resid[0, 0] + x[0,0] - x[-1,-1]
	resid[-1, Ny-1] = resid[0, Ny-2] + x[0, Ny-2] - x[-1, Ny-1]
	resid[Nx-1, Ny-1] = resid[Nx-2, Ny-2] + x[Nx-2, Ny-2] - x[Nx-1, Ny-1]
	resid[Nx-1, -1] = resid[Nx-2, 0] + x[Nx-2, 0] - x[Nx-1, -1]
	return resid


@njit
def sor_solver(x0, Nx, Ny, dx, dy, dt, omega, epsilon, u_star, v_star):
	contador=0
	xatual=x0
	while(contador<1000000):
		res=residuo(xatual, Nx, Ny, dx, dt, omega, u_star, v_star, 0.0, 0.0)
		xatual=xatual+res
		if(max(np.abs(res.flatten()))<epsilon):
			break
		contador=contador+1
	return xatual

@njit
def residuo2(x, Nx, Ny, dx, dt, omega, u, v, termo1, termo2):
	resid=np.copy(x)
	for i in range(1, Nx-1):
		for j in range(1, Ny-1):
			resid[i,j] = 0.25*(x[i+1,j] + x[i-1,j] + x[i, j+1] + x[i, j-1]) + (dx/4.0)*(v[i,j] - v[i-1,j]) - (dx/4.0)*(u[i,j] - u[i,j-1]) - x[i,j]
	return resid

@njit
def sor_solver2(x0, Nx, Ny, dx, dy, dt, omega, epsilon, u, v):
	contador=0
	xatual=np.copy(x0)
	while(contador<1000000):
		res=residuo2(xatual, Nx, Ny, dx, dt, omega, u, v, 0.0, 0.0)
		xatual=xatual+res
		if(max(np.abs(res.flatten()))<epsilon):
			break
		contador=contador+1
	return xatual

@njit
def resolve_cavidade2(Ra, Pr, tmax, U, dx, dy, dt, Nx, Ny, Nt, u, v, p, u_star, v_star, u_verdadeiro, v_verdadeiro, p_verdadeiro, psi, theta, thetaprojec, Nu, Num, thetaverdadeiro, termov, termou, c3, c4):
	#itero no tempo, exceto o tempo zero
	for k in range(1, Nt):
		#printo o tempo
		print('tempo: ', k*dt, '\n')
		#resolvo o u_star por um método explícito e também o v_star; 
		#naturalmente, itero apenas nos valores dentro da cavidade inicialmente e depois ajusto os ghost-points segundo as condições de contorno
		#a componente u é deslocada verticalmente (em y), logo não passo nem pelo ponto -1(Ny na memória) em y e nem pelo ponto Ny-1, logo o range é de 0 a Ny-1 (passo até o Ny-2)
		for i in range(Nx):
			for j in range(Ny-1):
				#é necessário verificar se estou nas paredes laterais
				if(i==0 or i==Nx-1):
					u_star[i, j] = 0
				else:
					#preciso projetar o termo da componente v na posição em que calculo o u, lembrando que não coincidem por conta da malha defasada
					termov = (v[k-1, i-1, j] + v[k-1, i-1, j+1] + v[k-1, i, j] + v[k-1, i, j+1])/4.0
					u_star[i,j] = u[k-1, i, j] + (dt*Pr)*((u[k-1, i+1, j] -2.0*u[k-1, i, j] + u[k-1, i-1, j])/(dx**2.0) + (u[k-1, i, j+1] -2.0*u[k-1, i, j] + u[k-1, i, j-1])/(dy**2.0)) -(dt*u[k-1, i, j]/(2.0*dx))*(u[k-1, i+1, j] - u[k-1, i-1, j]) -(dt*termov/(2.0*dy))*(u[k-1, i, j+1] - u[k-1, i, j-1])

		#após resolver o u_star, resolvo o v_star
		for i in range(Nx-1):
			for j in range(Ny):
				#é necessário verificar se estou nas paredes de baixo ou de cima
				if(j==0 or j==Ny-1):
					v_star[i,j] = 0
				else:
					#projeto o termo u
					termou = (u[k-1, i, j-1] + u[k-1, i+1, j-1] + u[k-1, i, j] + u[k-1, i+1, j])/4.0
					v_star[i,j] = v[k-1, i, j] + (dt*Pr)*((v[k-1, i+1, j] -2.0*v[k-1, i, j] + v[k-1, i-1, j])/(dx**2.0) + (v[k-1, i, j+1] -2.0*v[k-1, i, j] + v[k-1, i, j-1])/(dy**2.0)) -(dt*termou/(2.0*dx))*(v[k-1, i+1, j] - v[k-1, i-1, j]) -(dt*v[k-1, i, j]/(2.0*dy))*(v[k-1, i, j+1] - v[k-1, i, j-1]) + Ra*Pr*dt*(theta[k-1, i, j]+theta[k-1, i, j-1])/2.0

		#aplico as condições de contorno
		for i in range(Nx):
			u_star[i, Ny-1] = -u_star[i, Ny-2]
			u_star[i, -1] = -u_star[i, 0]

		#para o v
		for j in range(Ny):
			v_star[-1, j] = -v_star[0, j]
			v_star[Nx-1, j] = -v_star[Nx-2, j]

		#resolvo a pressão
		p[k] = sor_solver(p[k-1], Nx, Ny, dx, dy, dt, 1, 1.e-8, u_star, v_star)

		#resolvo o campo verdadeiro de velocidades, com as condições de contorno
		for i in range(Nx):
			for j in range(-1, Ny):
				if(i==0 or i==Nx-1):
					u[k,i,j] = 0
				else:
					u[k,i,j] = u_star[i,j] - dt*(p[k,i,j]-p[k,i-1,j])/dx
				# u[k,i,j] = u_star[i,j] - dt*(p[k,i,j]-p[k,i-1,j])/dx
		# for i in range(Nx):
		# 	u[k,i, Ny-1] = 2*U(i*dx) - u[k,i, Ny-2]
		# 	u[k,i, -1] = -u[k,i, 0]
		for i in range(-1, Nx):
			for j in range(Ny):
				if(j==0 or j==Ny-1):
					v[k,i,j] = 0
				else:
					v[k,i,j] = v_star[i,j] - dt*(p[k,i,j] - p[k,i,j-1])/dy
				# v[k,i,j] = v_star[i,j] - dt*(p[k,i,j] - p[k,i,j-1])/dy
		# for j in range(Ny):
		# 	v[k,-1, j] = -v[k,0, j]
		# 	v[k,Nx-1, j] = -v[k,Nx-2, j]

		#resolvo a temperatura
		for i in range(Nx-1):
			for j in range(Ny-1):
				c3=(u[k-1, i+1, j] + u[k-1,i,j])/2.0
				c4=(v[k-1,i,j+1] + v[k-1,i,j])/2.0
				theta[k, i, j] = theta[k-1, i, j] -dt*c3*(theta[k-1, i+1, j]-theta[k-1, i-1, j])/(2.0*dx) - dt*c4*(theta[k-1, i, j+1]-theta[k-1, i, j-1])/(2.0*dy) + dt*((theta[k-1, i+1, j] - 2.0*theta[k-1, i, j] +theta[k-1, i-1, j])/(dx**2.0) + (theta[k-1, i, j+1] - 2.0*theta[k-1, i, j] + theta[k-1, i, j-1])/(dy**2.0))

		#agora resolvo para os ghost-points da temperatura
		for j in range(-1, Ny):
			theta[k, -1, j] = 2.0-theta[k, 0, j]
			theta[k, Nx-1, j] = -theta[k, Nx-2, j]
		for i in range(Nx-1):
			theta[k, i, Ny-1] = theta[k, i, Ny-2]
			theta[k, i, -1] = theta[k, i, 0]

		#coloco os campos verdadeiros nas posições verdadeiras
		for i in range(Nx):
			for j in range(Ny):
				u_verdadeiro[k,i,j] = (u[k,i,j-1] + u[k,i,j])/2.0
		for i in range(Nx):
			for j in range(Ny):
				v_verdadeiro[k,i,j] = (v[k,i-1,j] + v[k,i,j])/2.0
		for i in range(Nx):
			for j in range(Ny):	    		
				p_verdadeiro[k,i,j] = 0.25*(p[k,i-1,j]+p[k,i-1,j-1]+p[k,i,j]+p[k,i,j-1])
				thetaverdadeiro[k,i,j] = 0.25*(theta[k,i-1,j]+theta[k,i-1,j-1]+theta[k,i,j]+theta[k,i,j-1])

		#projeto o theta dentro dos limites verticais
		for i in range(-1, Nx):
			for j in range(Ny):
				thetaprojec[k, i, j] = (theta[k, i, j] + theta[k, i, j-1])/2.0

		#calculo o vetor de Nu
		for j in range(Ny):
			Nu[k, j] = (thetaprojec[k, -1, j] - thetaprojec[k, 0, j])/(dx)
		#calculo o Num
		Num[k] = 0
		for j in range(Ny-1):
			Num[k] = Num[k] + dy*(Nu[k, j] + Nu[k, j+1])/2.0

		#calculo a função de corrente psi
		psi[k] = sor_solver2(psi[k-1], Nx, Ny, dx, dy, dt, 1, 0.5*1.e-8, u[k], v[k])


def resolve_cavidade(Ra, tmax, U, tamX, tamY):
	# #inicializo os passos  
	# dx = 0.5*(1.0/np.sqrt(Re))
	# dy = dx
	# dt = 0.2*Re*dx**2.0

	# #calculo as quantidades Nx, Ny, Nt
	# Nx = int((tamX/dx)+1)
	# Ny = int((tamY/dy)+1)
	# Nt = int((tmax/dt)+1)

	#alternativamente posso fixar as quantidades de pontos e calcular os passos
	Nx=int(20)
	Ny=int((tamY/tamX)*(Nx-1) + 1)
	dx = tamX/(Nx-1)
	dy = dx
	dt = 0.1*dx**2.0
	# dt = 1.e-3
	Nt = int((tmax/dt)+1)

	print(Nx, Ny, Nt, '\n')

	#defino o número de Prandtl
	Pr=0.71

	#inicializo os vetores
	u=np.zeros([Nt, Nx, Ny+1])
	v=np.zeros([Nt, Nx+1, Ny])
	p=np.zeros([Nt, Nx+1, Ny+1])
	u_star=np.zeros([Nx, Ny+1])
	v_star=np.zeros([Nx+1, Ny])
	u_verdadeiro=np.zeros([Nt, Nx, Ny])
	v_verdadeiro=np.zeros([Nt, Nx, Ny])
	p_verdadeiro=np.zeros([Nt, Nx, Ny])
	psi=np.zeros([Nt, Nx, Ny])
	theta = np.zeros([Nt, Nx+1, Ny+1])
	thetaprojec = np.zeros([Nt, Nx+1, Ny])
	Nu = np.zeros([Nt, Ny])
	Num = np.zeros([Nt])
	thetaverdadeiro = np.zeros([Nt, Nx, Ny])

	#inicializo a velocidade para a velocidade da parede na tampa superior
	#considero que para isso as velocidades imediatamente acima e abaixo da tampa superior se movem com a velocidade U
	for i in range(Nx):
		u[0, i, Ny-2]=0
		u[0, i, Ny-1]=2*U(i*dx)
		u_verdadeiro[0, i, Ny-2]=0
		u_verdadeiro[0, i, Ny-1]=2*U(i*dx)

	#É preciso inicializar também theta
	for j in range(-1, Ny):
		theta[0, 0, j] = 1
		theta[0, -1, j] = 1
		theta[0, Nx-2, j] = 0
		theta[0, Nx-1, j] = 0

	#chamo uma função mais otimizada para fazer o loop no tempo
	resolve_cavidade2(Ra, Pr, tmax, U, dx, dy, dt, Nx, Ny, Nt, u, v, p, u_star, v_star, u_verdadeiro, v_verdadeiro, p_verdadeiro, psi, theta, thetaprojec, Nu, Num, thetaverdadeiro, 0.0, 0.0, 0.0, 0.0)

	return(u_verdadeiro, v_verdadeiro, p, dx, dy, dt, Nx, Ny, Nt, u_star, v_star, p_verdadeiro, p, u, v, psi, thetaverdadeiro, Num)

u_verdadeiro,v_verdadeiro,p, dx, dy, dt, Nx, Ny, Nt, u_star, v_star, p_verdadeiro, p, u, v, psi, thetaverdadeiro, Num = resolve_cavidade(1.e3, 5, Urge2, 1.0, 1.0)

xs = dx*np.arange(Nx)

ys = dy*np.arange(Ny)

ts = dt*np.arange(Nt)

X, Y = np.meshgrid(xs,ys)

# print(u[0], ' \n')
# print(v[0], ' \n')
# print(u_verdadeiro[0], '\n')
# print(v_verdadeiro[0], '\n')

# print(u[1], ' \n')
# print(v[1], ' \n')
# print(u_verdadeiro[1], '\n')
# print(v_verdadeiro[1], '\n')

print(p_verdadeiro[Nt-1], '\n')

# plt.quiver(xs,ys,np.transpose(u_star[:, :Ny]),np.transpose(v_star[:Nx]))
# plt.show()

# plt.quiver(xs,ys,np.transpose(u[Nt-1, :, :Ny]),np.transpose(v[Nt-1, :Nx, :]))
# plt.show()

plt.quiver(xs, ys, np.transpose(u_verdadeiro[Nt-1]), np.transpose(v_verdadeiro[Nt-1]))
plt.show()

contours = plt.contour(xs, ys, np.transpose(psi[Nt-1]), colors = 'black')
plt.clabel(contours, inline=True, fontsize=15, colors = 'k')
plt.show()

# plt.imshow(np.transpose(p[Nt-1]), cmap='viridis')
# plt.colorbar()
# plt.show()

plt.imshow(np.flip(np.transpose(p_verdadeiro[Nt-1]), 0), cmap='viridis')
plt.colorbar()
plt.show()

plt.imshow(np.flip(np.transpose(thetaverdadeiro[Nt-1]), 0), cmap='hot')
plt.colorbar()
plt.show()

plt.plot(ts[1:Nt], Num[1:Nt])

plt.show()

# ax = plt.axes(projection='3d')
# ax.contour3D(X, Y, p_verdadeiro[Nt-1], 50, cmap='hot')
# ax.set_xlabel('x')
# ax.set_ylabel('y')
# ax.set_zlabel('z')
# ax.view_init(90, 0)
# plt.show()